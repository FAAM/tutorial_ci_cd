# tutorial_ci_cd
<a href="https://github.com/psf/black"><img alt="Code style: black" src="https://img.shields.io/badge/code%20style-black-000000.svg"></a>
[![pipeline status](https://gitlab.com/FAAM/tutorial_ci_cd/badges/master/pipeline.svg)](https://gitlab.com/FAAM/tutorial_ci_cd/-/commits/master)
[![coverage report](https://gitlab.com/FAAM/tutorial_ci_cd/badges/master/coverage.svg)](https://gitlab.com/FAAM/tutorial_ci_cd/-/commits/master)
<a href="https://faam.gitlab.io/tutorial_ci_cd/"><img alt="Link a la Documentación" src="https://img.shields.io/badge/docs-link-brightgreen"></a>

learning ci/cd integration with gitlab

