def sumar(x, y):
    """
    Funcion sumar
    :param x: primer argumento
    :param y: segundo argumento
    :return: suma de los dos argumentos
    """
    return x + y
